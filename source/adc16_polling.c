/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "fsl_debug_console.h"
#include "board.h"
#include "fsl_adc16.h"
#include "fsl_ftm.h"

#include "pin_mux.h"
#include "clock_config.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define ADC_CONVERTION (4096/3.3)
#define VEL_CONVERTION 0.259871979
#define SYS_DELAY 5
#define REFERENCIA 1.94//4.9
#define ERROR 1
#define Kp 2
#define Ki 0
#define Kd -10

#define DEMO_ADC16_BASE ADC0
#define DEMO_ADC16_CHANNEL_GROUP 0U
#define DEMO_ADC16_USER_CHANNEL 12U


/* The Flextimer instance/channel used for board */
#define BOARD_FTM_BASEADDR FTM0
#define BOARD_FTM_CHANNEL kFTM_Chnl_0
/* Interrupt number and interrupt handler for the FTM instance used */
#define FTM_INTERRUPT_NUMBER FTM0_IRQn
#define FTM_LED_HANDLER FTM0_IRQHandler
/* Interrupt to enable and flag to read; depends on the FTM channel used */
#define FTM_CHANNEL_INTERRUPT_ENABLE kFTM_Chnl0InterruptEnable
#define FTM_CHANNEL_FLAG kFTM_Chnl0Flag
/* Get source clock for FTM driver */
#define FTM_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_BusClk)


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*!
 * @brief delay a while.
 */
void delay(void);

/*******************************************************************************
 * Variables
 ******************************************************************************/
const uint32_t g_Adc16_12bitFullRange = 4096U;

volatile bool ftmIsrFlag          = false;
volatile bool brightnessUp        = true; /* Indicate LED is brighter or dimmer */
volatile uint8_t updatedDutycycle = 50U;

volatile uint32_t g_systickCounter;

/*******************************************************************************
 * Code
 ******************************************************************************/

void delay(void)
{
    volatile uint32_t i = 0U;
    for (i = 0U; i < 80000U; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

void FTM_LED_HANDLER(void)
{
    ftmIsrFlag = true;

//    if (brightnessUp)
//    {
//        /* Increase duty cycle until it reach limited value, don't want to go upto 100% duty cycle
//         * as channel interrupt will not be set for 100%
//         */
//        if (++updatedDutycycle >= 99U)
//        {
//            updatedDutycycle = 99U;
//            brightnessUp     = false;
//        }
//    }
//    else
//    {
//        /* Decrease duty cycle until it reach limited value */
//        if (--updatedDutycycle == 1U)
//        {
//            brightnessUp = true;
//        }
//    }

    if ((FTM_GetStatusFlags(BOARD_FTM_BASEADDR) & FTM_CHANNEL_FLAG) == FTM_CHANNEL_FLAG)
    {
        /* Clear interrupt flag.*/
        FTM_ClearStatusFlags(BOARD_FTM_BASEADDR, FTM_CHANNEL_FLAG);
    }
    __DSB();
}

void SysTick_Handler(void)
{
    if (g_systickCounter != 0U)
    {
        g_systickCounter--;
    }
}

void SysTick_DelayTicks(uint32_t n)
{
    g_systickCounter = n;
    while (g_systickCounter != 0U)
    {
    }
}

/*!
 * @brief Main function
 */
int main(void)
{
    adc16_config_t adc16ConfigStruct;
    adc16_channel_config_t adc16ChannelConfigStruct;

    /*________________________________________________*/
	ftm_config_t ftmInfo;
	ftm_chnl_pwm_signal_param_t ftmParam;
	ftm_pwm_level_select_t pwmLevel = kFTM_LowTrue;


    /* Configure ftm params with frequency 24kHZ */
    ftmParam.chnlNumber            = BOARD_FTM_CHANNEL;
    ftmParam.level                 = pwmLevel;
    ftmParam.dutyCyclePercent      = updatedDutycycle;
    ftmParam.firstEdgeDelayPercent = 0U;
    /*________________________________________________*/

    /* Board pin, clock, debug console init */
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();

    PRINTF("\r\nADC16 polling Example.\r\n");

    /*
     * adc16ConfigStruct.referenceVoltageSource = kADC16_ReferenceVoltageSourceVref;
     * adc16ConfigStruct.clockSource = kADC16_ClockSourceAsynchronousClock;
     * adc16ConfigStruct.enableAsynchronousClock = true;
     * adc16ConfigStruct.clockDivider = kADC16_ClockDivider8;
     * adc16ConfigStruct.resolution = kADC16_ResolutionSE12Bit;
     * adc16ConfigStruct.longSampleMode = kADC16_LongSampleDisabled;
     * adc16ConfigStruct.enableHighSpeed = false;
     * adc16ConfigStruct.enableLowPower = false;
     * adc16ConfigStruct.enableContinuousConversion = false;
     */
    ADC16_GetDefaultConfig(&adc16ConfigStruct);

#ifdef BOARD_ADC_USE_ALT_VREF
    adc16ConfigStruct.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
#endif

    ADC16_Init(DEMO_ADC16_BASE, &adc16ConfigStruct);
    ADC16_EnableHardwareTrigger(DEMO_ADC16_BASE, false); /* Make sure the software trigger is used. */

#if defined(FSL_FEATURE_ADC16_HAS_CALIBRATION) && FSL_FEATURE_ADC16_HAS_CALIBRATION
    if (kStatus_Success == ADC16_DoAutoCalibration(DEMO_ADC16_BASE))
    {
        PRINTF("ADC16_DoAutoCalibration() Done.\r\n");
    }
    else
    {
        PRINTF("ADC16_DoAutoCalibration() Failed.\r\n");
    }
#endif /* FSL_FEATURE_ADC16_HAS_CALIBRATION */

    PRINTF("ADC Full Range: %d\r\n", g_Adc16_12bitFullRange);
//    PRINTF("Press any key to get user channel's ADC value ...\r\n");

    adc16ChannelConfigStruct.channelNumber                        = DEMO_ADC16_USER_CHANNEL;
    adc16ChannelConfigStruct.enableInterruptOnConversionCompleted = false;

#if defined(FSL_FEATURE_ADC16_HAS_DIFF_MODE) && FSL_FEATURE_ADC16_HAS_DIFF_MODE
    adc16ChannelConfigStruct.enableDifferentialConversion = false;
#endif /* FSL_FEATURE_ADC16_HAS_DIFF_MODE */

	uint16_t adc_read_value = 0;
	float conver_value = 0;
	float veloci_value = 0;
	float error = 0;

	float g_last_U = 0;
	float g_last_error = 0;


	/*________________________________________________*/
	 FTM_GetDefaultConfig(&ftmInfo);
	/* Initialize FTM module */
	FTM_Init(BOARD_FTM_BASEADDR, &ftmInfo);

	FTM_SetupPwm(BOARD_FTM_BASEADDR, &ftmParam, 1U, kFTM_CenterAlignedPwm, 24000U, FTM_SOURCE_CLOCK);

	/* Enable channel interrupt flag.*/
	FTM_EnableInterrupts(BOARD_FTM_BASEADDR, FTM_CHANNEL_INTERRUPT_ENABLE);

	/* Enable at the NVIC */
	EnableIRQ(FTM_INTERRUPT_NUMBER);

	FTM_StartTimer(BOARD_FTM_BASEADDR, kFTM_SystemClock);
	/*________________________________________________*/

    /* Set systick reload value to generate 1ms interrupt */
    if (SysTick_Config(SystemCoreClock / 1000U))
    {
        while (1)
        {
        }
    }

	float UP = 0;
	float UI = 0;
	float UD = 0;
    while (1)
    {
//        GETCHAR();
//        /*
//         When in software trigger mode, each conversion would be launched once calling the "ADC16_ChannelConfigure()"
//         function, which works like writing a conversion command and executing it. For another channel's conversion,
//         just to change the "channelNumber" field in channel's configuration structure, and call the
//         "ADC16_ChannelConfigure() again.
//        */
        ADC16_SetChannelConfig(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct);
        while (0U == (kADC16_ChannelConversionDoneFlag & ADC16_GetChannelStatusFlags(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP)))
        {
        }
//        PRINTF("ADC Value: %d\r\n", ADC16_GetChannelConversionValue(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP));

        adc_read_value = ADC16_GetChannelConversionValue(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP);
        conver_value = adc_read_value / ADC_CONVERTION;
        veloci_value = conver_value / VEL_CONVERTION;
		error = REFERENCIA - veloci_value;

//		PRINTF("ADC Value: %d\r\n", adc_read_value);
//		PRINTF("Converter Value: %d V /100\r\n", ((int32_t)(conver_value*10000))/100);
//		PRINTF("Veloc Value: %d rad/s /100\r\n", ((int32_t)(veloci_value*10000))/100);
//		PRINTF("error Value: %d /100\r\n\n\n", ((int32_t)(error*10000))/100);


		if ((error >= ERROR) || (error <= -ERROR)) {

			UP = (Kp * error);
			UI = (g_last_U + Ki*SYS_DELAY*error);
			UD = (Kd * (error - g_last_error))/SYS_DELAY;


			updatedDutycycle -= (UP + UI + UD);
			g_last_U = (UI);
			g_last_error = error;



			if(updatedDutycycle > 99){
				updatedDutycycle = 99;
			}
			else if(updatedDutycycle < 1){
				updatedDutycycle = 1;
			}
		}
		/*________________________________________________*/
		/* Use interrupt to update the PWM dutycycle */
		if (true == ftmIsrFlag) {
			/* Disable interrupt to retain current dutycycle for a few seconds */
			FTM_DisableInterrupts(BOARD_FTM_BASEADDR, FTM_CHANNEL_INTERRUPT_ENABLE);
			ftmIsrFlag = false;
			/* Disable channel output before updating the dutycycle */
			FTM_UpdateChnlEdgeLevelSelect(BOARD_FTM_BASEADDR, BOARD_FTM_CHANNEL, 0U);

			/* Update PWM duty cycle */
			FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, BOARD_FTM_CHANNEL, kFTM_CenterAlignedPwm, updatedDutycycle);

			/* Software trigger to update registers */
			FTM_SetSoftwareTrigger(BOARD_FTM_BASEADDR, true);
			/* Start channel output with updated dutycycle */
			FTM_UpdateChnlEdgeLevelSelect(BOARD_FTM_BASEADDR, BOARD_FTM_CHANNEL, pwmLevel);
			/* Delay to view the updated PWM dutycycle */
			delay();
			/* Enable interrupt flag to update PWM dutycycle */
			FTM_EnableInterrupts(BOARD_FTM_BASEADDR, FTM_CHANNEL_INTERRUPT_ENABLE);
		}
		/*________________________________________________*/

		/* Delay 1 ms */
		SysTick_DelayTicks(SYS_DELAY);
    }
}
